<?php
 get_header();
$field = get_fields();
// Template name: Home Flex
?>

<?php
    if(have_rows('conteudo')) :
        foreach ($field['conteudo'] as $i => $conteudo):
            switch ($conteudo['acf_fc_layout']) {
                case 'cnt_banner':?>
                    <section class="slide cycle-slideshow" data-cycle-slides="> article" data-cycle-speed="800">
                        <?php foreach ($conteudo['rep_banner'] as $slide): ?>
                                <article class="slide__each" style="background-image: url('<?php echo $slide['img_banner']; ?>');">
                                    <div class="row">
                                        <div class="slide__content">
                                            <div class="slide__title-one"><?php echo $slide['ttl_banner']; ?></div>
                                            <div class="slide__title-two"><?php echo $slide['sttl_banner']; ?></div>
                                            <div class="slide__text"><?php echo $slide['ds_banner']; ?></div>
                                            
                                            <?php if( !empty($slide['botao']['ttl_botao_banner']) || !empty($slide['botao']['lnk_botao_banner'])  ): ?>
                                            
                                                <a href="<?php echo $slide['botao']['lnk_botao_banner']; ?>" title="" class="slide__more"><?php echo $slide['botao']['ttl_botao_banner']; ?></a>
                                            
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </article>
                                <div class="slide__pagination cycle-pager"></div>
                        <?php endforeach; ?>
                    </section>
                <?php break;
                case 'cnt_itens':?>
                    <!-- Three Itensasdf -->
                    <section class="three-itens">
                        <div class="row">
                            <article class="three-itens__each three-itens__each--one">
                                <img src="<?php echo $conteudo['gp_item1']['icn_item']; ?>" alt="" class="iconServicos">
                                <h3><?php echo $conteudo['gp_item1']['ttl_item']; ?></h3>
                                <p><?php echo $conteudo['gp_item1']['ds_item']; ?></p>
                            </article>
                            <article class="three-itens__each three-itens__each--two">
                                <img src="<?php echo $conteudo['gp_item2']['icn_item']; ?>" alt="" class="iconServicos">
                                <h3><?php echo $conteudo['gp_item2']['ttl_item']; ?></h3>
                                <p><?php echo $conteudo['gp_item2']['ds_item']; ?></p>
                            </article>
                            <article class="three-itens__each three-itens__each--three">
                                <img src="<?php echo $conteudo['gp_item3']['icn_item']; ?>" alt="" class="iconServicos">
                                <h3><?php echo $conteudo['gp_item3']['ttl_item']; ?></h3>
                                <p><?php echo $conteudo['gp_item3']['ds_item']; ?></p>
                            </article>
                        </div>
                    </section>

                <?php break;
                case 'cnt_sobre':?>
                <section class="about-us">
                    <article class="row">
                        <div class="about-us__person" style="background-image: url('<?php echo $conteudo['img_sobre']; ?>')"></div>
                        <div class="about-us__text">
                            <h4><?php echo $conteudo['ttl_sobre']; ?></h4>
                            <h3><?php echo $conteudo['sbt_sobre']; ?></h3>
                            <p><?php echo $conteudo['ds_sobre'] ?></p>
                            <ul>
                                <?php if( !empty($conteudo['lst_sobre'])): 
                                    foreach($conteudo['lst_sobre'] as $item):?>
                                    <li><?php echo $item['txt_sobre'] ?></li>
                                <?php endforeach;                                    
                                    endif; ?>
                            </ul>
                        </div>
                    </article>
                </section>
                <?php break;
                case 'cnt_depoimentos':?>
                    <!-- Testimonials -->
                    <section class="testimonials">
                        <div class="row align-center owl-carousel" id="testimonials">                                                        
                            <div class="testimonials__each">
                                <p><?php echo $conteudo['txt_depoimento']; ?></p>
                                <div class="row align-center">
                                    <div class="testimonials__photo">
                                        <img src="<?php echo $conteudo['img_autor']; ?>" alt="">                                        
                                        <p><?php echo $conteudo['nm_autor']; ?></p>
                                        <p><?php echo $conteudo['nm_profissao']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php break;
                case 'cnt_galeria':?>
                    <!-- Partners -->
                    <section class="partners">
                        <div class="partners__content owl-carousel galeria__content" id="partners">
                            <?php foreach($conteudo['fotos'] as $foto): ?>
                                <div>
                                    <a href="<?php echo $foto['url']; ?>">
                                        <img src="<?php echo $foto['url']; ?>" alt="Parceiro">
                                    </a>
                                    
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </section>
                <?php break;


            }
        endforeach;
    endif;


?>


<?php get_footer(); ?>

<?php
$field = get_fields();
get_header();
// Template name: Home
?>
    <!-- Slide -->
    <section class="slide cycle-slideshow" data-cycle-slides="> article" data-cycle-speed="800">

        <?php
        $slides = get_field('banner');
        foreach ($slides as $slide):?>

            <article class="slide__each" style="background-image: url(<?php echo $slide['imagem']; ?>);">
                <div class="row">
                    <div class="slide__content">
                        <div class="slide__title-one"><?php echo $slide['titulo-one']; ?></div>
                        <div class="slide__title-two"><?php echo $slide['titulo-two']; ?></div>
                        <div class="slide__text"><?php echo $slide['descricao']; ?></div>
                        <?php $botao = $slide['botao'] ?>
                        <?php if( !empty($botao['link']) || !empty($botao['titulo']) ): ?>
                            <a href="<?php echo $slide['botao']['link'] ?>" title="" class="slide__more"><?php echo $slide['botao']['titulo'] ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </article>

        <?php endforeach; ?>
        <div class="slide__pagination cycle-pager"></div>
    </section>

    <!-- Three Itens -->
    <section class="three-itens">
        <div class="row">
            <?php
            $item1 = get_field('item1');
            $item2 = get_field('item2');
            $item3 = get_field('item3');
            ?>
            <article class="three-itens__each three-itens__each--one">
                <img src="<?php echo $item1['icone']; ?>" alt="" class="iconServicos">
                <h3><?php echo $item1['titulo']; ?></h3>
                <p><?php echo $item1['descricao']; ?></p>
            </article>
            <article class="three-itens__each three-itens__each--two">
                <img src="<?php echo $item2['icone']; ?>" alt="" class="iconServicos">
                <h3><?php echo $item2['titulo']; ?></h3>
                <p><?php echo $item2['descricao']; ?></p>
            </article>
            <article class="three-itens__each three-itens__each--three">
                <img src="<?php echo $item3['icone']; ?>" alt="" class="iconServicos">
                <h3><?php echo $item3['titulo']; ?></h3>
                <p><?php echo $item3['descricao']; ?></p>
            </article>
        </div>
    </section>

    <!-- AboutUS -->
    <section class="about-us">
        <article class="row">
            <div class="about-us__person" style="background-image: url('http://fastmanga.com.br/wp-content/uploads/2016/09/person.png')"></div>
            <div class="about-us__text">
                <h4><?php echo $field['ttl']; ?></h4>
                <h3><?php echo $field['subttl']; ?></h3>
                <p><?php echo $field['desc']; ?></p>
                <ul>
                    <?php foreach ($field['lst_sobre'] as $sobre): ?>
                        <li><?php echo $sobre['conteudo'] ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </article>
    </section>

    <!-- Testimonials -->
    <section class="testimonials">
        <div class="row align-center owl-carousel" id="testimonials">
            <div class="testimonials__each">
                <p><?php echo the_field('depoimento') ?></p>
                <div class="row align-center">
                    <div class="testimonials__photo">
                        <img src="<?php echo the_field('icone') ?>" alt="">
                        <p><?php echo the_field('autor') ?></p>
                        <p><?php echo the_field('profissao') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Partners -->
    <section class="partners">
        <div class="partners__content owl-carousel galeria__content" id="partners">
            <?php foreach ($field['parceiros'] as $img): ?>
            <div>
                <a href="<?php echo $img['img_parceiro']; ?>">
                    <img src="<?php echo $img['img_parceiro']; ?>" alt="Parceiro">
                </a>
            </div>
            <?php endforeach; ?>
    </section>

<?php get_footer(); ?>
<?php
    $field = get_field('rodape', 'option');
?>
<footer>
    <div class="gotop">
        <div class="row align-right">
            <div class="gotop__content">
                <a href="#" data-target=".header" class="gotop goTop">subir</a>
            </div>
        </div>
    </div>

    <?php
        if($field ):            
            foreach ($field as $rodape):                
                switch ($rodape['acf_fc_layout']):
                    case 'cnt_contato':
                    
                        ?>

                        <section class="contact-box" id="contact-box" style="background-image: url('http://fastmanga.com.br/wp-content/uploads/2017/08/contact.jpg')">
                            <div class="row">
                                <article class="contact-box__info">
                                    <div class="contact-box__content">
                                        <h3>informações</h3>

                                        <div class="contact-box__box contact-box__box--padding contact-box__box--end">
                                            <span class="contact-box__title">endereço</span>
                                            <span class="contact-box__text"><?php echo $rodape['txt_endereco']; ?></span>
                                        </div>

                                        <div class="contact-box__box contact-box__box--padding contact-box__box--phone">
                                            <span class="contact-box__title">ligue para nós</span>
                                            <span class="contact-box__text"><a href="tel:13 3222-2222"><?php echo $rodape['txt_celular']; ?></a></span>
                                            <span class="contact-box__text"><a href="tel:13 3211-1111"><?php echo $rodape['txt_telefone']; ?></a></span>
                                        </div>

                                        <div class="contact-box__box contact-box__box--padding contact-box__box--email">
                                            <span class="contact-box__title">mande um email</span>
                                            <span class="contact-box__text"><a href="mailto:contato@seuemail.com.br"><?php echo $rodape['txt_email']; ?></a></span>
                                        </div>
                                    </div>
                                </article>
                                <article class="contact-box__info">
                                    <div class="contact-box__content">
                                        <h3>mande uma mensagem</h3>
                                        <div class="contact-box__box">
                                            <div role="form" class="wpcf7" id="wpcf7-f5-o1" lang="pt-BR" dir="ltr">
                                                <div class="screen-reader-response"></div>
                                                <?php echo do_shortcode('[contact-form-7 id="182" title="Formulário de Mensagem"]'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </section>


                        <?php


                    break;
                    case 'cnt_localizacao': ?>
                        <div class="maps">
                            <div class="maps__content">
                                <div class="marker" data-lat="<?php echo $rodape['mapa']['lat']; ?>" data-lng="<?php echo $rodape['mapa']['lng'];?>">
                                    <div class="maps__name">Fast Manga - Coporativo</div>
                                    <div class="maps__horario">horário: SEG a SEX das 8:30 as 18:00</div>
                                </div>
                            </div>
                        </div>
                        <?php 
                    break;
                endswitch; ?>
            <?php
            endforeach;
        endif;?>

    <!-- Copyright -->
    <div class="copyright">
        <div class="row">
            <div class="copyright__logo">
                <img src="<?php echo the_field('img_logo_rodape', 'option'); ?>" alt="Fast Manga - Coporativo" alt="Fast Manga - Coporativo">

            </div>
            <div class="copyright__text">
                <span><?php echo the_field('ttl_rodape', 'option'); ?></span>
            </div>
            <div class="copyright__manga">
                <a href="http://www.agenciamanga.com/" target="_blank"></a>
            </div>
        </div>
    </div>

</footer>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"http:\/\/fastmanga.com.br\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Verifique se voc\u00ea n\u00e3o \u00e9 um rob\u00f4."}}};
    /* ]]> */
</script>
<script type='text/javascript' src='http://fastmanga.com.br/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0'></script>
<script type='text/javascript' src='http://fastmanga.com.br/wp-content/themes/manga-corporativo/js/main.js?ver=1.0.0'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAw438cDd9LDKOZdD7VXxJKyCCRtzTZet4&#038;ver=1.0.0'></script>
<script type='text/javascript' src='http://fastmanga.com.br/wp-includes/js/wp-embed.min.js?ver=4.9.3'></script>
</body>
</html>
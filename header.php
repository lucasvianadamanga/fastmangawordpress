<?php $field = get_fields();?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=yes">
    <title>Página Inicial - Fast Manga - Coporativo</title>

    <!-- This site is optimized with the Yoast SEO plugin v6.2 - https://yoa.st/1yg?utm_content=6.2 -->
    <link rel="canonical" href="http://fastmanga.com.br/" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Página Inicial - Fast Manga - Coporativo" />
    <meta property="og:url" content="http://fastmanga.com.br/" />
    <meta property="og:site_name" content="Fast Manga - Coporativo" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Página Inicial - Fast Manga - Coporativo" />
    <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/fastmanga.com.br\/","name":"Fast Manga - Coporativo","potentialAction":{"@type":"SearchAction","target":"http:\/\/fastmanga.com.br\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    <!-- / Yoast SEO plugin. -->

    <link rel='dns-prefetch' href='//maps.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/fastmanga.com.br\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.3"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='contact-form-7-css'  href='http://fastmanga.com.br/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='css-css'  href='<?php echo get_template_directory_uri() ?>/style.css' type='text/css' media='all' />
    <script type='text/javascript' src='http://fastmanga.com.br/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='http://fastmanga.com.br/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <link rel='https://api.w.org/' href='http://fastmanga.com.br/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://fastmanga.com.br/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://fastmanga.com.br/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.3" />
    <link rel='shortlink' href='http://fastmanga.com.br/' />
    <link rel="alternate" type="application/json+oembed" href="http://fastmanga.com.br/wp-json/oembed/1.0/embed?url=http%3A%2F%2Ffastmanga.com.br%2F" />
    <link rel="alternate" type="text/xml+oembed" href="http://fastmanga.com.br/wp-json/oembed/1.0/embed?url=http%3A%2F%2Ffastmanga.com.br%2F&#038;format=xml" />
</head>
<body class="">
<header class="header">
    <div class="header__social">
        <div class="row">

            <div class="header__logo">
                <a href="http://fastmanga.com.br" title="Fast Manga - Coporativo">
                    <img src="<?php echo the_field('img_logo', 'options') ?>" alt="Fast Manga - Coporativo">
                </a>
            </div>

            <div class="header__info">
                <div class="header__social__phone">
                    <a href="tel:+<?php the_field('txt_telefone_header', 'options'); ?>"><?php the_field('txt_telefone_header', 'options'); ?></a>
                    <a href="tel:+<?php the_field('txt_celular_header', 'options'); ?>"><?php the_field('txt_celular_header', 'options'); ?></a>
                </div>

                <div class="header__social__link">
                    <a href="<?php echo $field['fb'] ?>" class="header__social__link--facebook" target="_blank"></a>

                    <a href="<?php echo $field['gp'] ?>" class="header__social__link--gplus" target="_blank"></a>

                    <a href="<?php echo $field['ttw'] ?>" class="header__social__link--twitter" target="_blank"></a>

                    <a href="<?php echo $field['yt'] ?>" class="header__social__link--youtube" target="_blank"></a>

                    <a href="<?php echo $field['lk'] ?>" class="header__social__link--linkedin" target="_blank"></a>

                    <a href="<?php echo $field['ig'] ?>" class="header__social__link--instagram" target="_blank"></a>
                </div>

                <div class="header__mobile">
                    <button class="header__mobile__btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>

        </div>
    </div><!-- Header Social -->
    <nav class="header__navigation">
        <div class="row">
            <div class="header__navigation__content">
                <?php
                $args = array(
                    'container-class' => 'menu-navegacao-container',
                    'menu-class' => 'menu',
                    'menu' => 'menu-navegacao'
                );
                wp_nav_menu($args);
                ?>
                <a href="tel:+<?php the_field('txt_telefone_header', 'options');; ?>" class="telefoneMobile"><?php the_field('txt_telefone_header', 'options');; ?>2</a>
                <a href="tel:+<?php the_field('txt_celular_header', 'options'); ?>" class="telefoneMobile"><?php the_field('txt_celular_header', 'options'); ?></a>

                <a href="mailto:<?php echo $field['conteudo'][5]['txt-email'] ?>" class="telefoneMobile"><?php echo $field['conteudo'][5]['txt-email'] ?></a>
            </div>

            <div class="header__social__link">
                <a href="<?php echo $field['fb'] ?>" class="header__social__link--facebook" target="_blank"></a>

                <a href="<?php echo $field['gp'] ?>" class="header__social__link--gplus" target="_blank"></a>

                <a href="<?php echo $field['ttw'] ?>" class="header__social__link--twitter" target="_blank"></a>

                <a href="<?php echo $field['yt'] ?>" class="header__social__link--youtube" target="_blank"></a>

                <a href="<?php echo $field['lk'] ?>" class="header__social__link--linkedin" target="_blank"></a>

                <a href="<?php echo $field['ig'] ?>" class="header__social__link--instagram" target="_blank"></a>
            </div>
        </div>
    </nav><!-- Header Navegation -->
</header><!-- Header -->